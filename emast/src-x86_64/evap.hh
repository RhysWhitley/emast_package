#ifndef _evaporate_RCPP_EVAPORATE_H
#define _evaporate_RCPP_EVAPORATE_H
#define _USE_MATH_DEFINES

#include <iostream>
#include <math.h>
#include <Rcpp.h>
#include "gridcell.hh"

using namespace std;
using namespace Rcpp;

// numerical constants used in evaporation calculations
    static const float
                d2r     = M_PI/180.0,
                cw      = 1.0,
                solc    = 1360.0,
                dtime   = 0.0036,
                nsecs   = 86400.0,
                albedo  = 0.17;
// single returns
    float cf_ARG    ( const int day     );
    float cf_SOLC   ( const int day     );
    float cf_DELT   ( const int day     );
    float cf_SLOPE  ( const float Tair  );
    float cf_HS     ( const float x,    const float y,  const float lat,    const float delta   );
// multiple returns
    void cf_H01     ( const float u,    const float v,  const float spl,    float &h0,      float &h1 );
    void cf_TABLE   ( const float temp, float &gamma,   float &lambda );
// main routines
    void evaporate  ( GridCell &gc, const float spl,  const int day );
    void waterBucket( GridCell &gc );

#endif
