#ifndef _main_RCPP_MAIN_H
#define _main_RCPP_MAIN_H
#define _USE_MATH_DEFINES

#include <iostream>
#include <vector>
#include <math.h>
#include <Rcpp.h>
#include "gridcell.hh"
#include "evap.hh"

using namespace std;
using namespace Rcpp;

static const unsigned int nvar = 15;    // the number of bioclimatic variables being returned

RcppExport SEXP OZSTASH         ( SEXP R_gtc, SEXP R_gpr, SEXP R_gsw );
NumericVector   RConvert        ( vector<GridCell> &gc );
void            assign_Rtotal   ( GridCell &gc, const unsigned long ll, NumericMatrix yGrid );
void            assign_Rmonth   ( GridCell &gc, const unsigned long ll, NumericMatrix mGrid, int (GridCell::*fget)(const int) const );
void            assign_Rmonth   ( GridCell &gc, const unsigned long ll, NumericMatrix mGrid, float (GridCell::*fget)(const int) const );

#endif
