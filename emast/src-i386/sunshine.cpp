#include "sunshine.hh"

RcppExport SEXP sunshine( SEXP r_sun ) {
//*****************************************************************
// A programme to calculate monthly radiation above atmosphere: rtoa
// (Total Radiation Above Atmosphere)
//*****************************************************************

    NumericMatrix   swr(r_sun);

    IntegerVector days =
        IntegerVector::create(31,28,31,30,31,30,31,31,30,31,30,31);
    const int   nmon = days.size();

    int     dn, ll, i, j, ncell = swr.nrow();
    float   rtoa;

    NumericMatrix   mrtoa( ncell, nmon );
    NumericMatrix   mrsun( ncell, nmon );

    // fill matrices with zeroes
    fill( mrtoa.begin(), mrtoa.end(), 0 );
    fill( mrsun.begin(), mrsun.end(), 0 );

    for( ll=0, dn=0; ll<ncell; ll++ )
        for( i=0; i<nmon; i++ )
            for( j=0; j<days[j]; j++ ) {
                ++dn;
                rtoa = TRA( dn, swr(ll,2) );
                mrtoa(ll,i) = rtoa/days(i) + mrtoa(ll,i);
            }
        mrsun(ll,i) = 200*( (swr(ll,i+2)/mrtoa(ll,i))-0.25 );

    return wrap(mrsun);

} // end program

//================================================================================

float TRA( const int day, const float lat ) {
// Calculates the total radiation at the top of the atmosphere based on
// latitude and day of the year.

    const float eccn = 0.01675,
                solc = 1360.,
                d2r  = M_PI/180.;

    float   arg, msolc, delta, x, y, hs,
            rtoa_Wm, rtoa_MJ;

    arg         = 360*day/365*d2r;
    msolc       = solc*(1+(2*eccn*cos(arg)));
    delta       = -23.4*cos(((day+10)*360)/365*d2r);
    x           = sin(lat*d2r)*sin(delta*d2r);
    y           = cos(lat*d2r)*cos(delta*d2r);
    if( x>y )
        hs = M_PI;
    else if( x<(-1.*y) )
        hs = 0;
    else
        hs = acos(-1*tan(lat*d2r)*tan(delta*d2r));
    rtoa_Wm     = msolc*((hs*x)+(sin(hs)*y))/M_PI;
    rtoa_MJ     = rtoa_Wm*86400*1e-6;

    return rtoa_MJ;

} // end TRA
