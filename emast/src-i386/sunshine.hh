#ifndef _sunshine_RCPP_SUNSHINE_H
#define _sunshine_RCPP_SUNSHINE_H
#define _USE_MATH_DEFINES

#include <iostream>
#include <math.h>
#include <Rcpp.h>
#include "R.h"

using namespace std;
using namespace Rcpp;

float TRA( const int day, const float lat );

RcppExport SEXP sunshine( SEXP r_sun );

#endif
