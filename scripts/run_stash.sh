#!/usr/bin/env Rscript

library(emast)
library(multicore)

message(">> Program started")

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

message(">> Reading data")
csv.files   <- list.files(pattern="^anuclim*")
in.data     <- multicore::mclapply( csv.files, read.csv, header=F, row.names=NULL, mc.preschedule=TRUE, mc.cores=4 )

message(">> Removing NA cells")
na.data     <- rapply( in.data, function(x) ifelse(x==-999,NA,x), how="replace" )
names(na.data) <- csv.files
# Remove NA data -- almost half the cells have no valuable information
temp.m      <- na.omit( do.call( cbind, na.data[[grep("mat",csv.files)]] ) )
ppt.m       <- na.omit( do.call( cbind, na.data[[grep("ppt",csv.files)]] ) )

message(">> Calculating")
gchar.m     <- cbind( temp.m[,1:2], elev=1, fc=150 )
system.time( bio_out <- grid.stash( temp.m, ppt.m, sun.data$SunHr, gchar.m ) )

message(">> Writing output files")
for( i in 1:length(bio_out) ) {
    write.table( bio_out[[i]], file=paste("~/Work/Research_Work/Ecosystem_Models/Alpha_Model/RCpp/data/stash_out_",names(bio_out)[[i]],".txt",sep=""), col.names=F, row.names=F )
}

