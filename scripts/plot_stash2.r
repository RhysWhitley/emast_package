library(emast)
library(raster)
library(maptools)

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

csv.files       <- list.files(pattern="STA.*.csv$")
in.data         <- lapply(csv.files, 
                      sqldf::read.csv.sql, 
                      sql="SELECT * FROM file WHERE V3 <> -999", 
                      header=F )
namesp1         <- do.call( rbind, sapply( csv.files, strsplit, split="\\.") )[,1]
names(in.data)  <- do.call( rbind, sapply( namesp1, strsplit, split=c("_")) )[,3]
# Convert to raster stacks for plotting
plot( stack.mon( in.data$Alpha ) )

plot( stack.dat( in.data$Total ) )


aushp <- readShapePoly("~/Work/Research_Work/GiS_Data/Australia/BOS/STE11aAust.shp",proj4string=CRS("+proj=longlat"))
# practice by sampling only part of Australia
ntshp <- subset( aushp, STATE_NAME=="Northern Territory" )
