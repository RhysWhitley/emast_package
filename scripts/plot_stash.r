library(emast)
library(raster)
library(maptools)

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

csv.files   <- list.files(pattern="^anuclim_5km_.*")
in.data     <- lapply(csv.files, 
                      sqldf::read.csv.sql, 
                      sql="SELECT * FROM file WHERE V3 <> -999", 
                      header=F )
names(in.data) <- csv.files

# create sunshine hours -- recursively convert to a list of data.frames
mat         <- in.data[[grep("mat",names(in.data))]]
ppt         <- in.data[[grep("ppt",names(in.data))]]
swr         <- in.data[[grep("swr",names(in.data))]]
sun         <- sunshine( swr[1:5000,] )
gchar       <- cbind( mat[,1:2], elev=1, fc=140 )
system.time( 
    bio_out <- grid.stash( mat[1:5000,], ppt[1:5000,], sun, gchar[1:5000,] ) 
    )

# Convert to raster stacks for plotting
#plot(  stack.mon( bio_out$AET ), zlim=c(0,300) )
#plot(  stack.mon( bio_out$PET ) )
#plot(  stack.dat( bio_out$Total ) )



#alp <- stack.mon( bio_out$Alpha )
#aet <- stack.mon( bio_out$AET )
#srad <- stack.mon( swr.m )
#rain <- stack.mon( ppt.m )
#tair <- stack.mon( temp.m )
#sunh <- stack.mon( sun.data$SunHr )
#rtoa <- stack.mon( sun.data$RadTop )


#aushp <- readShapePoly("~/Work/Research_Work/GiS_Data/Australia/BOS/STE11aAust.shp",proj4string=CRS("+proj=longlat"))
## practice by sampling only part of Australia
#tashp <- subset( aushp, STATE_NAME=="Tasmania" )
#taext <- extent( tashp )
#
#ta.tmp <- crop( temp, taext, snap="out" )
#ta.ppt <- crop( rain, taext, snap="out" )
#ta.sun <- crop( sunh, taext, snap="out" )
#
#plot( ta.ppt, col=rev(heat.colors(100)) )
#
## Tasmania only
#gchar       <- cbind( xyFromCell(ta.tmp,1:ncell(ta.tmp)), elev=1, fc=1 )
#ta.tmp.m    <- cbind( xyFromCell(ta.tmp,1:ncell(ta.tmp)), as.matrix(ta.tmp) )
#ta.ppt.m    <- cbind( xyFromCell(ta.ppt,1:ncell(ta.ppt)), as.matrix(ta.ppt) )
#ta.sun.m    <- cbind( xyFromCell(ta.sun,1:ncell(ta.sun)), as.matrix(ta.sun) )
#system.time( tas_out <- clim.stash( ta.tmp.m, ta.ppt.m, ta.sun.m, gchar ) )

#rain2$ppt   <- rowSums(  rain2[,grep("m",names(rain2))] )
#temp2$mat   <- rowMeans( temp2[,grep("m",names(temp2))] )
#
#mapcol      <- colorRampPalette( c("darkblue","blue","green","yellow", "orange","red") )
#
#
#txtcol <- 'black'
##x11( width=10, height=7.5 )
#pdf( file="anuclim30_temp.pdf", width=10, height=7.5 )
#par( mar=c(0.5,2,2,3), bty='n' )
#t.range <- c( floor(minValue(OZMAT)), ceiling(maxValue(OZMAT)) )
#plot( OZMAT, col=mapcol(80), axes=F, useRaster=T, add=F, asp=1, legend.width=1.4, legend.shrink=0.9,
#        axis.args=list(at=seq(t.range[1],t.range[2],2),
#                       labels=seq(t.range[1],t.range[2],2),
#                       col=txtcol,cex.axis=1.2,col.axis=txtcol ), 
#        legend.args=list(text=expression(MAT~~(degree*C)), col=txtcol, line=0.5, cex=1.2 ) )
#text( 112, -8.5, "MEAN ANNUAL TEMPERATURE", font=2, col=txtcol, adj=0, cex=1.9, xpd=T )
#plot( australia, col='gray', add=TRUE, lwd=1 )
#dev.off()
#
##x11( width=10, height=7.5 )
#pdf( file="anuclim30_rain.pdf", width=10, height=7.5 )
#par( mar=c(0.5,2,2,3), bty='n' )
##p.range <- c( floor(minValue(OZPPT)), ceiling(maxValue(OZPPT)) )
#p.range <- c( 0, 4000 )
#plot( OZPPT, col=rev(mapcol(800)), axes=F, useRaster=T, add=F, asp=1, legend.width=1.4, legend.shrink=0.9, 
#        axis.args=list(at=seq(p.range[1],p.range[2],500),
#                       labels=seq(p.range[1],p.range[2],500),
#                       col=txtcol,cex.axis=1.2,col.axis=txtcol ), 
#        legend.args=list(text=expression(PPT~~(mm)), col=txtcol, line=0.5, cex=1.2 ) )
#text( 112, -8.5, "ANNUAL TOTAL PRECIPITATION", font=2, col=txtcol, adj=0, cex=1.9, xpd=T )
#plot( australia, col='gray', add=TRUE, lwd=1 )
#dev.off()
## replicated into 1000 grids >> change cell size to test performance
#cells   <- 1e3
#latlon  <- t( array( c(140,-30), c(2,cells) ) )
#g_tair  <- cbind( latlon, t(array( rep(t_tair,cells), c(12,cells) ) ) )
#g_ppt   <- cbind( latlon, t(array( rep(t_ppt,cells) , c(12,cells) ) ) )
#g_swr   <- cbind( latlon, t(array( rep(t_swr,cells) , c(12,cells) ) ) )
#
#system.time( bio_out <- clim.stash( g_tair, g_ppt, g_swr ) )
