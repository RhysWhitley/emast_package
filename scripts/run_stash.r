library(emast)
library(raster)
library(maptools)

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

csv.files   <- list.files(pattern="tas.*.csv")
in.data     <- lapply( csv.files, read.csv, header=F, row.names=NULL )

temp    <- na.omit( in.data[[grep("mat",csv.files)]] )
prec    <- na.omit( in.data[[grep("ppt",csv.files)]] )
fsun    <- na.omit( in.data[[grep("sun",csv.files)]] )

##temp    <- in.data[[grep("mat",csv.files)]]
##prec    <- in.data[[grep("ppt",csv.files)]]
##fsun    <- in.data[[grep("sun",csv.files)]]

gchar   <- cbind( fsun[,1:2], elev=1, fc=1 )

system.time( bio_out <- clim.stash( temp, prec, fsun, gchar ) )


# change all -999 values to NA for plotting
#all.data    <- c( in.data, sun.data )
#na.data     <- rapply( all.data, f=function(x) ifelse(x==-999,NA,x), how="replace" )
#
# Convert each vector of data into a monthly raster
make.map <- function(X,i,cres) {
    xymin   <- sapply( X[,1:2], min )
    xymax   <- sapply( X[,1:2], max )
    xycell  <- ( xymax-xymin )/cres
    aumap       <- raster( nrow=xycell[1], ncol=xycell[2],
                   xmn=xymin[1],xmx=xymax[1],ymn=xymin[2],ymx=xymax[2],
                   crs="+proj=longlat +datum=WGS84" )
    coord       <- cellFromXY( aumap, cbind(X[[1]],X[[2]]))
    aumap[coord]<- X[[i+2]]
    return( aumap )
}
stack.mon <- function( dat, cres=0.05 ) {
    s <- stack()
    for( i in 1:(length(dat)-2) ) {
        r <- make.map(dat,i,cres)
        s <- addLayer( s, r )
    }
    names(s) <- c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
    return(s)
}
stack.dat <- function( dat, cres=0.05 ) {
    s <- stack()
    for( i in 1:(length(dat)-2) ) {
        r <- make.map(dat,i,cres)
        s <- addLayer( s, r )
    }
    names(s) <- c("AET","EET","PET","DET","FAPAR","MI","Alpha","MAT","MAP","FSUN","RO","GDD0","GDD5","GDD10","Chill")
    return(s)
}

# Convert to raster stacks for plotting
tot <- stack.dat( bio_out$Total )
eet <- stack.mon( bio_out$EET )
plot( tot, col=rev(rainbow(100)) )

