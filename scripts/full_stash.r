#!/usr/bin/env Rscript

library(emast)
library(multicore)

message(">> Program started")

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

message(">> Reading data")
csv.files   <- list.files(pattern="^anuclim.*.csv$")
in.data     <- multicore::mclapply( csv.files, read.csv, header=F, row.names=NULL, mc.preschedule=TRUE, mc.cores=4 )

message(">> Removing NA cells")
na.data     <- rapply( in.data, function(x) ifelse(x==-999,NA,x), how="replace" )
names(na.data) <- csv.files
# Remove NA data -- almost half the cells have no valuable information
temp.m      <- na.omit( do.call( cbind, na.data[[grep("mat",csv.files)]] ) )
ppt.m       <- na.omit( do.call( cbind, na.data[[grep("ppt",csv.files)]] ) )
swr.m       <- na.omit( do.call( cbind, na.data[[grep("swr",csv.files)]] ) )

if( sum(grep("sun",csv.files)) ) {
    message(">> Sunshine hours present\n")
    sun.m   <- na.omit( do.call( cbind, na.data[[grep("sun",csv.files)]] ) )
} else {
# create sunshine hours -- recursively convert to a list of data.frames
    message(">> Determining sunshine hours\n")
    sun.data    <- sunshine( swr.m )
    write( sun.data, file=paste("anuclim_5km_sun.csv",sep=","), col.names=F, row.names=F )
    sun.m   <- sun.data$SunHr
}

message(">> Running STASH")
gchar.m     <- cbind( temp.m[,1:2], elev=1, fc=150 )
system.time( bio_out <- grid.stash( temp.m, ppt.m, sun.m, gchar.m ) )

message(">> Writing output files")
for( i in 1:length(bio_out) ) {
    write.table( bio_out[[i]], file=paste("STASH_OUT_",names(bio_out)[[i]],".txt",sep=""), col.names=F, row.names=F )
}

