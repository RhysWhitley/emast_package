#!/usr/bin/env Rscript

library(emast)
library(raster)
library(maptools)

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

csv.files   <- list.files(pattern="*.csv")
in.data     <- lapply( csv.files, read.csv, header=F, row.names=NULL )
# create sunshine hours -- recursively convert to a list of data.frames
sun.data    <- sunshine( in.data[[grep("[Rr]ad",csv.files)]] )

# change all -999 values to NA for plotting
all.data    <- c( in.data, sun.data )
na.data     <- rapply( all.data, f=function(x) ifelse(x==-999,NA,x), how="replace" )

# Convert each vector of data into a monthly raster
make.map <- function(X,i) {
    aumap       <- raster( nrow=695,ncol=822,
                   xmn=112.9,xmx=154,ymn=-43.75,ymx=-9,
                   crs="+proj=longlat +datum=WGS84" )
    coord       <- cellFromXY( aumap, cbind(X[[1]],X[[2]]))
    aumap[coord]<- X[[i+2]]
    return( aumap )
}
stack.em <- function( dat ) {
    s <- stack()
    for( i in 1:(length(dat)-2) ) {
        r <- make.map(dat,i)
        s <- addLayer( s, r )
    }
    names(s) <- c("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")
    return(s)
}

# Convert to raster stacks for plotting
temp <- stack.em( na.data[[1]] )
srad <- stack.em( na.data[[2]] )
rain <- stack.em( na.data[[3]] )
sunh <- stack.em( na.data[[4]] )

aushp <- readShapePoly("~/Work/Research_Work/GiS_Data/Australia/BOS/STE11aAust.shp",proj4string=CRS("+proj=longlat"))
# practice by sampling only part of Australia
tashp <- subset( aushp, STATE_NAME=="Tasmania" )
taext <- extent( tashp )

ta.tmp <- crop( temp, taext, snap="out" )
ta.ppt <- crop( rain, taext, snap="out" )
ta.sun <- crop( sunh, taext, snap="out" )

ll <- xyFromCell(ta.tmp,1:ncell(ta.tmp))
write.table( cbind(ll,as.matrix(ta.tmp)), "tas_mat.csv", sep=",", col.names=F, row.names=F )
write.table( cbind(ll,as.matrix(ta.ppt)), "tas_ppt.csv", sep=",", col.names=F, row.names=F )
write.table( cbind(ll,as.matrix(ta.sun)), "tas_sun.csv", sep=",", col.names=F, row.names=F )

