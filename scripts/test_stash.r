library(klimat)

# wave function for a year
wave <- function(x,a=1e3,b=2e2,k=0.53,p=1.5) a+b*sin(x*k+p)
mons <- 1:12

# modified by unit values for specific drivers
t_tair  <- wave(mons, a=20, b=30, k=0.52)
t_ppt   <- wave(mons, k=0.50, p=4.75)
t_swr   <- wave(mons, k=0.54)/1.5e3

# a plotting function to observe the inputs
#tsplot <- function( tdat, tcol='black' ) {
#    par( new=T, mar=c(4,8.5,1,4.5) )
#    plot( tdat, type='o', pch=19, cex=0.8, col=tcol, xaxt='n', yaxt='n', xlab="", ylab="", bty='n' )
#}
#if( dev.cur()==1 ) {
#   x11( width=7, height=4 )
#} else {
#   dev.off()
#}
#tsplot( t_tair, tcol='DarkRed' )
#    mtext( expression(T[air]~~(degree*C)), 2, line=2.5, cex=1 )
#    axis( 2, at=seq(-20,60,10), col='gray', las=1, cex.axis=0.9 )
#tsplot( t_ppt,  tcol='Orange'  )
#    mtext( expression(PPT~~(mm)), 2, line=6.9, cex=1 )
#    axis( 2, at=seq(0,2e3,100), col='gray', line=4.25, las=1, cex.axis=0.9 )
#tsplot( t_swr,  tcol='Salmon'  )
#    mtext( expression(f[sun]~~(hr~mn^-1)), 4, line=3, cex=1 )
#    axis( 1, at=1:12, col='gray' ); mtext( "Month", 1, line=2.7, cex.axis=0.9 )
#    axis( 4, at=seq(0,1,0.05), col='gray', las=1, cex.axis=0.9 )
#legend( "center", c(expression(T[air]),expression(PPT),expression(f[sun])), col=c('DarkRed','Orange','Salmon'),
#       bty='n', lty=1, pch=19 )

# replicated into 1000 grids >> change cell size to test performance
cells   <- 1e3
latlon  <- t( array( c(140,-30), c(2,cells) ) )
g_tair  <- cbind( latlon, t(array( rep(t_tair,cells), c(12,cells) ) ) )
g_ppt   <- cbind( latlon, t(array( rep(t_ppt,cells) , c(12,cells) ) ) )
g_swr   <- cbind( latlon, t(array( rep(t_swr,cells) , c(12,cells) ) ) )

system.time( bio_out <- clim.stash( g_tair, g_ppt, g_swr ) )
