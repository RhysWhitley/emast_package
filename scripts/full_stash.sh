#!/usr/bin/env Rscript

library(emast)

message(">> Program started")

setwd("~/Work/Research_Work/Climatologies/Australia/mean30yr/")

message(">> Reading data")
csv.files   <- list.files(pattern="^anuclim.*.csv$")
in.data     <- lapply(csv.files, 
                      sqldf::read.csv.sql, 
                      sql="SELECT * FROM file WHERE V3 <> -999", 
                      header=F )
names(in.data) <- csv.files

# create sunshine hours -- recursively convert to a list of data.frames
mat         <- in.data[[grep("mat",names(in.data))]]
ppt         <- in.data[[grep("ppt",names(in.data))]]
swr         <- in.data[[grep("swr",names(in.data))]]

#if( sum(grep("sun",csv.files)) ) {
#    message(">> Sunshine hours present\n")
#    sun.m       <- in.data[[grep("sun",csv.files)]]
#} else {
## create sunshine hours -- recursively convert to a list of data.frames
    message(">> Determining sunshine hours\n")
    sun         <- sunshine( swr )$SunHr
#    write.table( sun.m, file="anuclim_5km_sun.csv", sep=",", col.names=F, row.names=F )
#}

message(">> Running STASH")
gchar       <- cbind( mat[,1:2], elev=1, fc=140 )
system.time( 
    bio_out <- grid.stash( mat, ppt, sun, gchar ) 
    )

message(">> Writing output files")
for( i in 1:length(bio_out) ) {
    write.table( bio_out[[i]], file=paste("STASH_OUT_",names(bio_out)[[i]],".csv",sep=""), sep=",", col.names=F, row.names=F )
}
