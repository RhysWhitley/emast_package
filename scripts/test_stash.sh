#!/usr/bin/env Rscript
library(emast)

# wave function for a year
wave <- function(x,a=1e3,b=2e2,k=0.53,p=1.5) a+b*sin(x*k+p)
mons <- 1:12

# modified by unit values for specific drivers
t_tair  <- wave(mons, a=20, b=30, k=0.52)
t_ppt   <- wave(mons, k=0.50, p=4.75)
t_swr   <- wave(mons, k=0.54)/1.5e3

# replicated into grids
cells   <- 1e1
# geo-coord is 140 deg E x -30 deg N
latlon  <- t( array( c(140,-30), c(2,cells) ) )
# elevation 10 m above sea-level and field capacity 150 mm
g_char  <- cbind( latlon, t( array( c(10,150), c(2,cells) ) ) )
# create a matrix that repeats the wave functions foe each cell
g_tair  <- cbind( latlon, t(array( rep(t_tair,cells), c(12,cells) ) ) )
g_ppt   <- cbind( latlon, t(array( rep(t_ppt,cells) , c(12,cells) ) ) )
g_swr   <- cbind( latlon, t(array( rep(t_swr,cells) , c(12,cells) ) ) )

# do calc and time it
tim <- system.time( bio_out <- clim.stash( g_tair, g_ppt, g_swr, g_char ) ); tim


# plot tests below here
