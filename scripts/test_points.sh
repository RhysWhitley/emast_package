#!/usr/bin/env Rscript
library(emast)

tair <- rep(24,10)
ppt <- rep(150,10)
sun <- seq( 0.1, 1.0, length=10 )
lat <- rep(-30,10)
mon <- 1:10
message("\ntesting point.stash\n")
point.stash( tair, ppt, sun, mon, lat )

message("now with NA values")
point.stash( tair, -9999, sun, mon, lat )


message("\n\ntesting sunray\n")
sunray( 15:30, 1, 12 )

message("now with NA values")
sunray( rep(-9999,8), 1, 12 )
