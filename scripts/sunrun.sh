#!/usr/bin/env Rscript
options( max.print=120 )
library(emast)

cells <- 1e3
tm <- matrix( runif(120,10,30), cells, 12 )
ll <- t( array(c(140,-30), c(2,cells) ) )

test <- cbind( ll, tm )
sunshine(test)
