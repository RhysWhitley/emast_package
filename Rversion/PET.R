## insolation is determined by the following three factors: 
## orbital paramters, latitude and the date of the year
## In this version, orbital paramters are explicit for palaeo application

## note: unit in radians, rather than degree

PET <- function(lat,elv,dtc,clou,epsilon,e,phi_ref)

{

## ----------------------------------------------------  
##      1: calculate solar declination (delta) 
## ----------------------------------------------------  

# define the orbital parameter

	ve <- 81                    # vernal equinox
	dn <- 365                   # total number of days in a year
	solc <- 1360
	albedo <- 0.17

	phi_p <- phi_ref + pi/2     # phi_p is the longitude of perihelion with respect to December solstice

# convert the unit of latitude from degree to radians

	lat <- lat*pi/180
	
# NT: requires now cloudiness as ratio (<1.0)! (i.e dcl=clou/100 (mts))

	dcl <- clou/100

# define the date of the year

	D <- seq(1,dn,1)


# define the function of "lint" to generate a linear interpolation for daily phi (Kutzbach and Gallimore, 1988, JGR93: 803-821)

	lint<-function(y,t,phi) # y is t at each angle day, x is the phi value correspondingly
	{
     
# get a sequence for phi range, starting from -81*pi/180
# due to the vernal equinox as the 81th day of a year,
# rather than the first day of a year     
     
     	phi <- seq(-ve*pi/180,2*pi-ve*pi/180,pi/180)   
     
# calculate t (the day angle) with a more accurate equation
     
    	 t <- phi -2*e*(cos(phi-phi_p)-cos(phi_p))
 
# interpolation for each day "y" with the nearest lower and the nearest higher t values
     
   	     b2<-t[min(which(t>=y))]
     	 b1<-t[max(which(t<y))]
     	 a2<-phi[min(which(t>=y))]
     	 a1<-phi[max(which(t<y))]
     
# to get the corresponding x (phi) for each day "y"
     
     	 x<-(y-b1)*(a2-a1)/(b2-b1)+a1
     
     	 return(x)
	 }

# day angle

     y <- (2*pi)/dn*(D-ve) 

# the phi value corresponding to each day angle using the lint interoplated function

     phi <-unlist(lapply(y,lint))   
 
# daily solar declination (delta), as a matrix with rows for grids and columns for days

     delta<-asin(sin(epsilon)*sin(phi))


## -------------------------------------------------------------------------------  
##      2: calculate instantaneous solar radiation above atmosphere (rtoa)
## -------------------------------------------------------------------------------  


# inverse relative Sun-Earth distance

     dr<-1-2*e*sin(phi-phi_p)

# modified solar constant (msolc)

     msolc <- solc* dr 

     hs <- acos(-1*(tan(lat)%*%(t(tan(delta)))))     # acos(-tan(lat)*tan(delta))      
      
     x <- sin(lat)%*%t(sin(delta))      # sin(lat)*sin(delta), calculated as matrix since lat(rows) and delta(cols) are vector 
     y <- cos(lat)%*%t(cos(delta))
     
     hs[x>y] <- pi
     hs[x<(-1*y)] <- 0

# total radiation above atmosphere at any hour during the day (rtoa), restored in a matrix[latitudes,days]

     rtoa <- msolc*(hs*(sin(lat)%*%t(sin(delta)))+(cos(lat)%*%t(cos(delta)))*sin(hs))/pi  # unit as w/m2 ( =J/s/m2 )

## -------- daily total radiation above atmosphere (dRtoa), ignoring the variation of temperature through the day

     dRtoa <- rtoa*86400*(10^(-6)) # unit as MJ/m2/day

## --------  daily net downward shortwave radiation with elevation effect "1+0.027*elv*10^(-3)" on transmittivity "0.75"

     dRsw <- dRtoa*(0.25+0.5*dcl)*(1-albedo)*(1+0.027*elv*10^(-3)) # unit as MJ/m2/day

## --------  daily longwave radiation

     dRl <- (0.2+0.8*dcl)*(107-dtc)

## --------  daily PAR as 50% of daily shortwave radiation

     dPar <- 0.5*dRsw/0.22  # unit as mol/m2／day

## --------  daily net radiation

     dRn <- dRsw-dRl

## -------------------------------------------------------------------------------  
##      3: calculate daily equilibrium evaportranspiration (dpet)
## -------------------------------------------------------------------------------  

# instantaneously: equilibrium evapotranspiration = Rn* lambda* sat/ (sat+gamma) ----- Priestley & Taylor (1972)

# gamma is the psychrometer constant; lambda is the latent heat of water vaporisation, both are used in the Clausius-Ckapeuron function, describing the relationship between temperature and saturated vapour pressure (sat). 
# The methods for estimating gamma,lambda and sat are based on FAO documents (http://www.fao.org/docrep/X0490E/x0490e07.htm).

P <- 101.3*((293-0.0065*elv)/293)^5.26    # P is atmosphere pressure in kPa, elv is elevation in m
gamma <- 0.665*P    # psychrometric constant in Pa/0C
lambda <- 2.45   # latent heat of vaporization in MJ/kg
sat <- 4098*0.6108*10^(3)*exp(17.296*dtc/(237.3+dtc))/((237.3+dtc)^2)   # the slope of saturation vapour pressure curve in Pa/0C

# integral instantaneous pet to daily pet (elevation effect on transmittivity included)

uu <- (msolc*(0.25+0.5*dcl)*(1+0.027*elv*10^(-3))*(1-albedo)*(sin(lat)%*%t(sin(delta)))-(0.2+0.8*dcl)*(107-dtc))
vv <- (msolc*(0.25+0.5*dcl)*(1+0.027*elv*10^(-3))*(1-albedo)*(cos(lat)%*%t(cos(delta))))

u <- (0.0036/lambda)*(sat/(sat+gamma))*uu
v <- (0.0036/lambda)*(sat/(sat+gamma))*vv

arg <- -(u/v)
h0 <- acos(arg) # h0 is the point in time when net radiation crosses zero
h0[u>=v] <- pi
h0[u<=-v] <- 0

dpet <- 2*((u*h0)+(v*sin(h0)))/(pi/12)  # daily equilibrium evapotranspiration

dcon <- (0.0036/lambda)*(sat/(sat+gamma))*(0.2+0.8*dcl)*(107-dtc) # condensation (dew)

return(list(dpet,dPar,dcon,v,u,h0))  # return the outputs to main

}

