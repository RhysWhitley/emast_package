##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ read input climatological data ~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# ***SET THE PATH FOR DATA and program FILES HERE***

path<-"/Users/42432669/Desktop/R-bioclimate/Bioclimate/Code/"

file<-paste(path,"input.txt",sep="")

data <- read.delim(file,header=T)
lat <- data[,1]
lon <- data[,2]
elv <- data[,3]
mtc <- data[,4:15]
mcl <- data[,16:27]   # monthly fractional sunshine
mpr <- data[,28:39] 

grid <- nrow(data)

# a general summary of each variables read from the the input data as a check 

summary(data)


##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ call for daily function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source(paste(path,"daily.r",sep=""))

# linear interoplating monthly climate input data to daily arrays

temp <- daily(mtc,grid)  # temp is daily temperature and restored as a matrix: row for each grid, col for each day   
clou <- daily(mcl,grid)  # sunshine percentage
prec <- daily(mpr,grid)


##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ parameterization ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# setting field capacity

fcap<-150

# setting orbital parameters:

epsilon <- 23.4*pi/180      # tilt
e <- 0.01675                # eccentricity
phi_ref <- 284*pi/180       # longitude of perihelion


####################################################################
####################################################################

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ start calculating bioclimatic variables ~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


##------------------- call for PET function: calculating daily equilibirum evapotranspiration (dpet) and daily photosynthetically active radiation (dPar)

source(paste(path,"PET.r",sep=""))

outpet<-PET(lat,elv,temp,clou,epsilon,e,phi_ref)                 

dpet <- matrix(data=unlist(outpet[1]),nrow=nrow(data))          # daily equilibrium evapotranspiration
dPar <- matrix(data=unlist(outpet[2]),nrow=nrow(data))          # daily photosynthetically active radiation
dcon <- matrix(data=unlist(outpet[3]),nrow=nrow(data))			# daily condensation (dew)

# output the daily results

write.table(cbind(lon,lat,dpet),"/Users/42432669/Desktop/dpet.txt",sep="\t",row.name=F,col.name=F)
write.table(cbind(lon,lat,dPar),"/Users/42432669/Desktop/dPar.txt",sep="\t",row.name=F,col.name=F)
write.table(cbind(lon,lat,dcon),"/Users/42432669/Desktop/dcon.txt",sep="\t",row.name=F,col.name=F)
write.table(cbind(lon,lat,v),"/Users/42432669/Desktop/v.txt",sep="\t",row.name=F,col.name=F)
write.table(cbind(lon,lat,u),"/Users/42432669/Desktop/u.txt",sep="\t",row.name=F,col.name=F)
write.table(cbind(lon,lat,h0),"/Users/42432669/Desktop/h0.txt",sep="\t",row.name=F,col.name=F)

##------------------- call for gddpar function: calculate growing degree days (gdd) and photosynthetically active radiation (par) above certain temperature (e.g. 0 and 5) based on a daily step

source(paste(path,"gddpar.r",sep=""))

outgddpar0 <- gddpar(temp,dPar,0) # above 0 degree
outgddpar5 <- gddpar(temp,dPar,5) # above 5 degree

gdd0 <- matrix(data=unlist(outgddpar0[1]),nrow=nrow(data)) # accumulated growing degree days above 0◦C 
gdday0 <- matrix(data=unlist(outgddpar0[2]),nrow=nrow(data)) # length of growing degree days above 0◦C 
mgdd0 <- matrix(data=unlist(outgddpar0[3]),nrow=nrow(data)) # mean growing degree days above 0◦C 
par0 <- matrix(data=unlist(outgddpar0[4]),nrow=nrow(data)) # total photosynthetically active radiation during the period above 0◦C 
mpar0 <- matrix(data=unlist(outgddpar0[5]),nrow=nrow(data)) # mean photosynthetically active radiation during the period above 0◦C

gdd5 <- matrix(data=unlist(outgddpar5[1]),nrow=nrow(data)) # accumulated growing degree days above 5◦C 
gdday5 <- matrix(data=unlist(outgddpar5[2]),nrow=nrow(data)) # length of growing degree days above 5◦C 
mgdd5 <- matrix(data=unlist(outgddpar5[3]),nrow=nrow(data)) # mean growing degree days above 5◦C 
par5 <- matrix(data=unlist(outgddpar5[4]),nrow=nrow(data)) # total photosynthetically active radiation during the period above 5◦C 
mpar5 <- matrix(data=unlist(outgddpar5[5]),nrow=nrow(data)) # mean photosynthetically active radiation during the period above 5◦C

##------------------- call for AET function: calculating daily actual evapotranspiration (dpet) 
## need to finish the "AET" code

source(paste(path,"AET.r",sep=""))

## ------------------ call for MV function: sum up daily values to monthly values

source(paste(path,"monthly.r",sep=""))

mpar<-MV(dPar) # monthly par

# ------------------ call for TS function:seasonality and timing for precipitation

source(paste(path,"seasonality.r",sep=""))

Pt <- matrix(data=unlist(TS(mpr)[1]),nrow=nrow(data))
Ps <- matrix(data=unlist(TS(mpr)[2]),nrow=nrow(data))



##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ calculate other basic bioclimatic variables ~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# mean annual temperature (MAT), mean (actually is total) annual precip (MAP)

MAT <- apply(mtc,1,mean)
MAP <- apply(mpr,1,sum)

# annual equilibrium evapotranspiration

yrpet <- apply(dpet,1,sum) 
yraet <- apply(daet,1,sum) 

# moisture index

MI <- MAP/yrpet


####################################################################
####################################################################

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ plot results ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

##------------------- call for MAP function: map the distribution pattern of the bioclimatic variables

source(paste(path,"maps.r",sep=""))

lonlat <- data[,2:1]    # lonlat: 1 column-longitude, 2 column-latitude. latitude increase firstly, and then longitude increase when latitudes are the same

# set your own breakpoints and colors for the bioclimatic variables

breakpoints <- c(0,5,10,15,20,25,35)
colors <- terrain.colors(7)[7:2]
            
MAP(lonlat,mgdd0,breakpoints,colors,1,"mgdd0")
MAP(lonlat,mgdd5,breakpoints,colors,1,"mgdd5")

breakpoints <- c(10,25,30,35,40,50)
colors <- terrain.colors(5)[5:1]

MAP(lonlat,mpar5,breakpoints,colors,1,"mpar5")
MAP(lonlat,mpar0,breakpoints,colors,1,"mpar0")

breakpoints <- c(0,500,750,1000,1250,2000)
colors <- terrain.colors(5)[5:1]
MAP(lonlat,mpar,breakpoints,colors,12,"mpar_")  


##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~ output results ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

write.table(cbind(lon,lat,par0,par5,gdd0,gdd5,mgdd0,mgdd5,gdday0,gdday5,MI,yrpet,MAT,MAP),"/Users/42432669/Desktop/Bioclim.txt",sep="\t",row.name=F,col.name=F)